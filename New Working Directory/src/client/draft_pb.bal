import ballerina/grpc;

public type CaliBlockingClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function addRecord(recordIn req, grpc:Headers? headers = ()) returns ([recordOut, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("Cali/addRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<recordOut>result, resHeaders];
        
    }

    public remote function updateRec(oldRec req, grpc:Headers? headers = ()) returns ([newRec, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("Cali/updateRec", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<newRec>result, resHeaders];
        
    }

    public remote function getRec(ID req, grpc:Headers? headers = ()) returns ([recordOut, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("Cali/getRec", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<recordOut>result, resHeaders];
        
    }

};

public type CaliClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "non-blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function addRecord(recordIn req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali/addRecord", req, msgListener, headers);
    }

    public remote function updateRec(oldRec req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali/updateRec", req, msgListener, headers);
    }

    public remote function getRec(ID req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("Cali/getRec", req, msgListener, headers);
    }

};

public type recordIn record {|
    string 'record = "";
    
|};


public type recordOut record {|
    string genre = "";
    string[] artist = [];
    string album = "";
    string[] tracklist = [];
    string date = "";
    int id = 0;
    
|};


public type oldRec record {|
    int oldId = 0;
    
|};


public type newRec record {|
    string genre = "";
    string[] artist = [];
    string album = "";
    string[] tracklist = [];
    string date = "";
    int id = 0;
    
|};


public type ID record {|
    int id = 0;
    
|};



const string ROOT_DESCRIPTOR = "0A0B64726166742E70726F746F22220A087265636F7264496E12160A067265636F726418012001280952067265636F72642291010A097265636F72644F757412140A0567656E7265180120012809520567656E726512160A06617274697374180220032809520661727469737412140A05616C62756D1803200128095205616C62756D121C0A09747261636B6C6973741804200328095209747261636B6C69737412120A0464617465180520012809520464617465120E0A02696418062001280552026964221E0A066F6C6452656312140A056F6C64496418012001280552056F6C644964228E010A066E657752656312140A0567656E7265180120012809520567656E726512160A06617274697374180220032809520661727469737412140A05616C62756D1803200128095205616C62756D121C0A09747261636B6C6973741804200328095209747261636B6C69737412120A0464617465180520012809520464617465120E0A0269641806200128055202696422140A024944120E0A0269641801200128055202696432640A0443616C6912220A096164645265636F726412092E7265636F7264496E1A0A2E7265636F72644F7574121D0A0975706461746552656312072E6F6C645265631A072E6E657752656312190A0667657452656312032E49441A0A2E7265636F72644F7574620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "draft.proto":"0A0B64726166742E70726F746F22220A087265636F7264496E12160A067265636F726418012001280952067265636F72642291010A097265636F72644F757412140A0567656E7265180120012809520567656E726512160A06617274697374180220032809520661727469737412140A05616C62756D1803200128095205616C62756D121C0A09747261636B6C6973741804200328095209747261636B6C69737412120A0464617465180520012809520464617465120E0A02696418062001280552026964221E0A066F6C6452656312140A056F6C64496418012001280552056F6C644964228E010A066E657752656312140A0567656E7265180120012809520567656E726512160A06617274697374180220032809520661727469737412140A05616C62756D1803200128095205616C62756D121C0A09747261636B6C6973741804200328095209747261636B6C69737412120A0464617465180520012809520464617465120E0A0269641806200128055202696422140A024944120E0A0269641801200128055202696432640A0443616C6912220A096164645265636F726412092E7265636F7264496E1A0A2E7265636F72644F7574121D0A0975706461746552656312072E6F6C645265631A072E6E657752656312190A0667657452656312032E49441A0A2E7265636F72644F7574620670726F746F33"
        
    };
}

