import ballerina/log;
import ballerina/mongodb;

public function main() {

    mongodb:ClientConfig mongoConfig = {
        host: "localhost",
        port: 27017,
        username: "admin",
        password: "admin",
        options: {sslEnabled: false, serverSelectionTimeout: 5000}
    };

    mongodb:Client mongoClient = checkpanic new (mongoConfig);
    mongodb:Database mongoDatabase = checkpanic mongoClient->getDatabase("Ballerina");
    mongodb:Collection mongoCollection = checkpanic mongoDatabase->getCollection("projects");

    map<json> doc1 = { "name": "ballerina", "type": "src" };
    map<json> doc2 = { "name": "connectors", "type": "artifacts" };
    map<json> doc3 = { "name": "docerina", "type": "src" };
    map<json> doc4 = { "name": "test", "type": "artifacts" };

    log:printInfo("------------------ Inserting Data -------------------");
    checkpanic mongoCollection->insert(doc1);
    checkpanic mongoCollection->insert(doc2);
    checkpanic mongoCollection->insert(doc3);
    checkpanic mongoCollection->insert(doc4);
  
    log:printInfo("------------------ Counting Data -------------------");
    int count = checkpanic mongoCollection->countDocuments(());
    log:printInfo("Count of the documents '" + count.toString() + "'.");


    log:printInfo("------------------ Querying Data -------------------");
    map<json>[] jsonRet = checkpanic mongoCollection->find(());
    log:printInfo("Returned documents '" + jsonRet.toString() + "'.");

    map<json> queryString = {name: "connectors" };
    jsonRet = checkpanic mongoCollection->find(queryString);
    log:printInfo("Returned Filtered documents '" + jsonRet.toString() + "'.");


    log:printInfo("------------------ Updating Data -------------------");
    map<json> replaceFilter = { "type": "artifacts" };
    map<json> replaceDoc = { "name": "main", "type": "artifacts" };

    int response = checkpanic mongoCollection->update(replaceDoc, replaceFilter, true);
    if (response > 0 ) {
        log:printInfo("Modified count: '" + response.toString() + "'.") ;
    } else {
        log:printInfo("Error in replacing data");
    }

   log:printInfo("------------------ Deleting Data -------------------");
   map<json> deleteFilter = { "name": "ballerina" };
   int deleteRet = checkpanic mongoCollection->delete(deleteFilter, true);
   if (deleteRet > 0 ) {
       log:printInfo("Delete count: '" + deleteRet.toString() + "'.") ;
   } else {
       log:printInfo("Error in deleting data");
   }

     mongoClient->close();
}
