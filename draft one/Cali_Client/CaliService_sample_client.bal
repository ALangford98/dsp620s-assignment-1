import ballerina/log;
import ballerina/grpc;
import ballerina/io;

public function main (string mode) {

CaliServiceBlockingClient blockingEp = new("http://localhost:9090");

 if (mode == "add") {

      log:printInfo("---------------------Add a new Artist------------------");
     }
        RecordInfo info = {
                date: "22/10/2020",
                artists: [
                    {
                    name: "Winston Marshall",
                    member: "yes"
                    },
                    {
                    name: "Ben Lovett",
                    member: "yes"
                    },
                    {
                    name: "Baaba Maal",
                    member: "no"
                    }
                ],
                band: "Mumford & Sons",
                songs: [
                    {
                        title: "There will be time",
                        genre: "folk rock",
                        platform: "Deezer"
                    }
                ]
            }
                           
    var addResponse = ArtistBlockingEp->addArtist(RecordInfo);
        if (addResponse is error) {
         log:printError("Error from Connector: " + addResponse.reason().toString());
    } else {
        string result;
         grpc:Headers resHeaders;
         [result, resHeaders] = addResponse;
         log:printInfo("Response - " + result + "\n");
        [hashCode, grpc:Headers] result = check blockingEp->writeRecord(info);
        io:println("A record has Successfully been added");
	    io:println("HASH KEY : ",result);

    } if (mode == "update") {

    } else if (mode == "view") {
         log:printInfo("Viewing a Record information");
         var getResponse = BlockingEp-> getArtists ("mkmk45587");
         if (getResponse is error) {
        log:printError("Error from Connector: " + getResponse.reason().toString());
     } else {
         string result;
        grpc:Headers resHeaders;
        [result, resHeaders] = getResponse;
        log:printInfo("Response - " + result + "\n");
     }
 } else {
        io:println("Unsupported operation mode entered!");
        return;
    }

    
}


