import ballerina/grpc;

public type CaliServiceBlockingClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(RecordInfo req, grpc:Headers? headers = ()) returns ([hashCode, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("CaliService/writeRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<hashCode>result, resHeaders];
        
    }

    public remote function updateRecord(hashCode req, grpc:Headers? headers = ()) returns ([RecordInfo, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("CaliService/updateRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<RecordInfo>result, resHeaders];
        
    }

    public remote function viewRecord(hashCode req, grpc:Headers? headers = ()) returns ([RecordInfo, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("CaliService/viewRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<RecordInfo>result, resHeaders];
        
    }

};

public type CaliServiceClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "non-blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(RecordInfo req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("CaliService/writeRecord", req, msgListener, headers);
    }

    public remote function updateRecord(hashCode req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("CaliService/updateRecord", req, msgListener, headers);
    }

    public remote function viewRecord(hashCode req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("CaliService/viewRecord", req, msgListener, headers);
    }

};

public type Songs record {|
    string title = "";
    string genre = "";
    string platform = "";
    string album = "";
    
|};


public type Artists record {|
    string name = "";
    string member = "";
    
|};


public type RecordInfo record {|
    string key = "";
    string date = "";
    Artists[] artists = [];
    Songs[] tracklist = [];
    string band = "";
    
|};


public type hashCode record {|
    string key = "";
    
|};



const string ROOT_DESCRIPTOR = "0A0A43616C692E70726F746F22650A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D12140A05616C62756D1804200128095205616C62756D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D6265722290010A0A5265636F7264496E666F12100A036B657918012001280952036B657912120A046461746518022001280952046461746512220A076172746973747318032003280B32082E4172746973747352076172746973747312240A09747261636B6C69737418042003280B32062E536F6E67735209747261636B6C69737412120A0462616E64180520012809520462616E64221C0A0868617368436F646512100A036B657918012001280952036B65793282010A0B43616C695365727669636512250A0B77726974655265636F7264120B2E5265636F7264496E666F1A092E68617368436F646512260A0C7570646174655265636F726412092E68617368436F64651A0B2E5265636F7264496E666F12240A0A766965775265636F726412092E68617368436F64651A0B2E5265636F7264496E666F620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "Cali.proto":"0A0A43616C692E70726F746F22650A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D12140A05616C62756D1804200128095205616C62756D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D6265722290010A0A5265636F7264496E666F12100A036B657918012001280952036B657912120A046461746518022001280952046461746512220A076172746973747318032003280B32082E4172746973747352076172746973747312240A09747261636B6C69737418042003280B32062E536F6E67735209747261636B6C69737412120A0462616E64180520012809520462616E64221C0A0868617368436F646512100A036B657918012001280952036B65793282010A0B43616C695365727669636512250A0B77726974655265636F7264120B2E5265636F7264496E666F1A092E68617368436F646512260A0C7570646174655265636F726412092E68617368436F64651A0B2E5265636F7264496E666F12240A0A766965775265636F726412092E68617368436F64651A0B2E5265636F7264496E666F620670726F746F33"
        
    };
}

