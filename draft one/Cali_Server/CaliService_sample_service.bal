import ballerina/grpc;
import ballerina/io;
import ballerina/log;
import ballerina/crypto;

map <RecordInfo> ArtistID = {};
string RecordInfo = ArtistID.key;
RecordInfo[ArtistID.key] = RecordInfo;

listener grpc:Listener ep = new (9090);

service CaliService on ep {

    resource function writeRecord(grpc:Caller caller, RecordInfo value) {
        // Implementation goes here.
        byte[] hash = [];
        byte[] data = [];

        //check to see if song exists
        if(ArtistID.hasKey(value.toString())){
            log:printError("The Song already Exists");
            string message = "The Song enter already exists";
			error? result_ = caller->send(message);
        }else{
            ArtistID[value.toString()] = <@untainted> value; 
            hash = value.toString().toBytes();
            data = crypto:hashSha256(hash);
            io:println("hash code is : " + data.toBase16());
            error? result = caller->send(data.toBase16()); //Checking for exception
            result = caller->complete();
            if(result is error){
                log:printError("Failed Hash Key : " + result.reason().toString());
            }else{
                value.key = data.toBase16(); 
				io:println("================= New Records added ===========================");
				io:println("Key Assigned : ", value.key + "Date         : ", value.date);
				io:println("Artists      : ", value.artists);
				io:println("Band         : ", value.band + "Songs        : ", value.songs);
            }
        }

    }
        // You should return a hashCode
    
    resource function updateRecord(grpc:Caller caller, hashCode value) {
        // Implementation goes here.
        
        string payload = "RecordInfo created : " + ArtistID;
        error? result = caller->send(payload);
        result = caller->complete();
        if (result is error) {
        log:printError("Error from Connector: " + result.reason().toString());
        // You should return a string
        }
    }
        // You should return a RecordInfo
    
    resource function viewRecord(grpc:Caller caller, hashCode value) {
        // Implementation goes here.
        
        string payload = "";
        error? result = ();
       if (ArtistID.hasKey(ArtistID)) {
        var jsonValue = json.constructFrom(RecordInfo[ArtistId]);
            if (jsonValue is error) {
                result = caller->sendError(grpc:INTERNAL,// internal error
                   <string>jsonValue.detail().message);
            } else {
                json ArtistDetails = jsonValue;
                payload = ArtistDetails.toString();
                result = caller->send(payload);
                result = caller->complete();
            }
       } else {
            // if the ID not found 
            payload = "Artist record : " + ArtistID + " cannot be found.";
           result = caller->sendError(grpc:NOT_FOUND, payload);
       }
        if (result is error) {
           log:printError("Error from server: " + result.reason().toString());
        }    
        // You should return a string
    }

        // You should return a RecordInfo
    }


public type Songs record {|
    string title = "";
    string genre = "";
    string platform = "";
    string album = "";
    
|};

public type Artists record {|
    string name = "";
    string member = "";
    
|};

public type RecordInfo record {|
    string key = "";
    string date = "";
    Artists[] artists = [];
    Songs[] songs = [];
    string band = "";
    
|};

public type hashCode record {|
    string key = "";
    
|};



const string ROOT_DESCRIPTOR = "0A0A43616C692E70726F746F22650A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D12140A05616C62756D1804200128095205616C62756D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D6265722290010A0A5265636F7264496E666F12100A036B657918012001280952036B657912120A046461746518022001280952046461746512220A076172746973747318032003280B32082E4172746973747352076172746973747312240A09747261636B6C69737418042003280B32062E536F6E67735209747261636B6C69737412120A0462616E64180520012809520462616E64221C0A0868617368436F646512100A036B657918012001280952036B65793282010A0B43616C695365727669636512250A0B77726974655265636F7264120B2E5265636F7264496E666F1A092E68617368436F646512260A0C7570646174655265636F726412092E68617368436F64651A0B2E5265636F7264496E666F12240A0A766965775265636F726412092E68617368436F64651A0B2E5265636F7264496E666F620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "Cali.proto":"0A0A43616C692E70726F746F22650A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D12140A05616C62756D1804200128095205616C62756D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D6265722290010A0A5265636F7264496E666F12100A036B657918012001280952036B657912120A046461746518022001280952046461746512220A076172746973747318032003280B32082E4172746973747352076172746973747312240A09747261636B6C69737418042003280B32062E536F6E67735209747261636B6C69737412120A0462616E64180520012809520462616E64221C0A0868617368436F646512100A036B657918012001280952036B65793282010A0B43616C695365727669636512250A0B77726974655265636F7264120B2E5265636F7264496E666F1A092E68617368436F646512260A0C7570646174655265636F726412092E68617368436F64651A0B2E5265636F7264496E666F12240A0A766965775265636F726412092E68617368436F64651A0B2E5265636F7264496E666F620670726F746F33"
        
    };
}

